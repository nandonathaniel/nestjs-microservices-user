import { Inject, Injectable, NotFoundException, forwardRef} from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }
}
