import { InjectRepository } from '@nestjs/typeorm';
import { Injectable} from '@nestjs/common';
import { DataSource, ILike, Raw, Repository } from 'typeorm';
import { UserRequestDto } from './dto/user-request.dto';
import { User, UserRole } from './entities/user.entity';

@Injectable()
export class UserRepository {
    @InjectRepository(User)
    private usersRepository: Repository<User>;

    constructor(db: DataSource) {
        this.usersRepository = db.getRepository(User);
    }

    async clear() {
        console.log("user ke clear");
        const allUsers = await this.usersRepository.find();
        await this.usersRepository.remove(allUsers);
    }

    delete(id: number) {
        return this.usersRepository.softDelete(id);
    }

    findAll() {
        return this.usersRepository.find({ select: ['id', 'username', 'name', 'role'] });
    }

    findAllWithoutAdmin() {
        return this.usersRepository.find({where: { role:  UserRole.Member}, select: ['id', 'username', 'name', 'role'] });
    }

    create(userRequestDto: UserRequestDto, password: string) {
        const user = this.usersRepository.create({...userRequestDto, password});
        return this.usersRepository
        .save(user)
        .then(({ password, ...result }) => result);
    }

    update(id: number, userRequestDto: UserRequestDto) {
        const result = this.usersRepository.update({ id }, userRequestDto)
        return result;
    }

    findOneId(id: number) {
        return this.usersRepository.findOne({ where: { id : id }, select: ['id', 'username', 'name', 'role'] });
    }

    findOneUsernameExceptPassword(username: string) {
        return this.usersRepository.findOne(
            {
                where: { 
                    username : Raw(alias => `BINARY ${alias} = :username`, { username })
                }, 
                select: ['id', 'username', 'name', 'role']
            }
        );
    }

    findOneUsername(username: string) {
        return this.usersRepository.findOne(
            { 
                where: { 
                    username : Raw(alias => `BINARY ${alias} = :username`, { username })
                }
            }
        );
    }
}

