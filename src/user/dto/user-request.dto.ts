import { ApiProperty } from "@nestjs/swagger";
import { UserRole } from "../entities/user.entity";

export class UserRequestDto {
    @ApiProperty({
        example : 'exampleUsername'
    })
    username: string;
    @ApiProperty({
        example : 'examplePassword'
    })
    password: string;
    @ApiProperty({
        example : 'exampleName'
    })
    name: string;

    @ApiProperty({ 
        example: UserRole.Member
    })
    role: UserRole;
}
