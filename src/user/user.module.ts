import { Module, forwardRef } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRepository } from './user.repository';
import { User } from './entities/user.entity';
import { BlogRepository } from '../utils/blog.repository';
import { Blog } from './entities/blog.entity';

@Module({
  imports: [TypeOrmModule.forFeature([User]), TypeOrmModule.forFeature([Blog])],
  controllers: [UserController],
  providers: [UserService, UserRepository, BlogRepository /*RolesGuard, {
    provide: APP_GUARD,
    useClass: RolesGuard,
  },*/],
  exports: [UserService, UserRepository, BlogRepository]
})
export class UserModule {}
