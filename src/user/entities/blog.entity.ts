
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn, DeleteDateColumn } from 'typeorm';
import { User } from './user.entity';

@Entity()
export class Blog {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User, { onDelete: 'CASCADE' })
  @JoinColumn({name:"userId",referencedColumnName:"id"})
  user: User;

  @Column()
  userId: number

  @Column()
  title: string;

  @Column()
  content: string;

  @Column({nullable: true})
  imageUrl: string;

  @Column({nullable: true})
  imageFileName: string;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  createdDate: Date;

  @Column({default: true})
  isPublished: Boolean;

  @DeleteDateColumn()
  deletedAt?: Date;
}