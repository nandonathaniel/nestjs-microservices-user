import { Inject, Injectable, NotFoundException, forwardRef} from '@nestjs/common';
import { UserRequestDto } from './dto/user-request.dto';
import { UserRepository } from './user.repository';
import { encodePassword } from '../utils/bcrypt';
import { BlogRepository } from '../utils/blog.repository';
import { RpcException } from '@nestjs/microservices';

@Injectable()
export class UserService {
  constructor(
    private usersRepository: UserRepository,
    private blogsRepository: BlogRepository
  ) {}
  async create(userRequestDto: UserRequestDto) {
    const password = await encodePassword(userRequestDto.password);
    return this.usersRepository.create(userRequestDto, password);
  }

  findAll(){
    return this.usersRepository.findAll();
  }

  findAllWithoutAdmin(){
    return this.usersRepository.findAllWithoutAdmin();
  }

  async findOneId(id: number) {
    const result = await this.usersRepository.findOneId(id);
    if(!result){
      throw new RpcException(
        new NotFoundException("User not found")
      );
    }
    return result;
  }

  async findOneUsername(username: string){
    const result = await this.usersRepository.findOneUsernameExceptPassword(username);
    if (!result) {
      throw new RpcException(
        new NotFoundException("User not found")
      );
    }
    console.log(result);
    return result;
  }

  async findOneUsernameWithPassword(username: string){
    const result = await this.usersRepository.findOneUsername(username);
    console.log(result);
    if (!result) {
      throw new NotFoundException('User not found');
    }
    return result;
  }

  async remove(id: number){
    const result = await this.usersRepository.delete(id);
    if(result.affected == 0) {
      throw new RpcException(
        new NotFoundException("User not found")
      );
    }
    else {
      const blogs = await this.blogsRepository.getAllBlogsByUser(id);
      for (const blog of blogs) {
        await this.blogsRepository.delete(blog.id);
      }
      return "Deleted User with id " + id.toString();
    }
    
  }

  async update(id: number, isAdmin: boolean, isSameUser: boolean, userRequestDto: UserRequestDto) {
    if(userRequestDto.password){
      userRequestDto.password = await encodePassword(userRequestDto.password);
    }

    const result = await this.usersRepository.update(id, userRequestDto);
    if(result.affected == 0) {
      throw new RpcException(
        new NotFoundException("User not found")
      );
    }
    else {
      return this.findOneId(id);
    }
  }
}
