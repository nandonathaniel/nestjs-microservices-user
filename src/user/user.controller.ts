import { Controller, Get, Post, Body, Patch, Param, Delete, BadRequestException, ParseIntPipe, ValidationPipe, UseGuards, Req, UnauthorizedException, NotFoundException } from '@nestjs/common';
import { UserService } from './user.service';
import { MessagePattern, RpcException } from '@nestjs/microservices';
import { UserRequestDto } from '../dto/user-request.dto';
import { UserRole } from './enums/role.enum';


@Controller()
export class UserController {
  constructor(private readonly userService: UserService) {}

  @MessagePattern('createUser')
  create(userRequestDto: UserRequestDto) {
    console.log(userRequestDto);
    if (!userRequestDto.username || !userRequestDto.password || !userRequestDto.name || !userRequestDto.role) {
      throw new BadRequestException('Missing required properties');
    }
    const alphanumericRegex = /^(?=.*[a-zA-Z])[a-zA-Z0-9]+$/;
    if (!alphanumericRegex.test(userRequestDto.username)) {
      throw new BadRequestException('Username must be alphanumeric with at least one alphabetical character');
    }
    return this.userService.create(userRequestDto);
  }

  @MessagePattern('findOneUserId')
  findById(id: number) {
    const resp = this.userService.findOneId(id);
    return resp;
  }

  @MessagePattern('findOneUserUsername')
  findByUsername(data) {
    const regex = /^[a-zA-Z0-9]*[a-zA-Z][a-zA-Z0-9]*$/;
    if (!regex.test(data.username)) {
      return new Error('Invalid username');
    }
    return data.withPassword ? this.userService.findOneUsernameWithPassword(data.username) : this.userService.findOneUsername(data.username);
  }

  @MessagePattern('findAllUser') 
  findAllUser(isAdmin: boolean) {
    return isAdmin ? this.userService.findAll() : this.userService.findAllWithoutAdmin();
  }

  @MessagePattern('updateUser')
  update(data) {
    // console.log("Dede");
    // console.log(data.id);
    // console.log(data.isAdmin);
    // console.log(data.isSameUser);
    if (!data.isAdmin && !data.isSameUser) {
      return  new UnauthorizedException('You are not authorized to update this user.');
    }

    if (!data.isAdmin && data.userRequestDto.role) {
      data.userRequestDto.role = UserRole.Member;
    }

    return this.userService.update(data.id, data.isAdmin, data.isSameUser, data.userRequestDto);
  }

  @MessagePattern('deleteUser')
  remove(id: number) {
    return this.userService.remove(id);
  }
}
