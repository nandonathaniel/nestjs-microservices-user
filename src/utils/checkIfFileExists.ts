import * as fs from 'fs';

async function checkIfFileExists(filePath: string): Promise<boolean> {
    return fs.existsSync(filePath);
}

export async function checkAndMove(imageId: string): Promise<boolean> {
    const sourcePath = `storage/temporary/${imageId}`;
    const targetPath = `storage/persistent/${imageId}`;
    if(checkIfFileExists(sourcePath)) {
        await fs.promises.rename(sourcePath, targetPath);
        return true;
    }
    else{
        return false;
    }
}