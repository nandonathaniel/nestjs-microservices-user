import { Injectable} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DataSource, Repository } from 'typeorm';
import { Blog } from '../user/entities/blog.entity';
@Injectable()
export class BlogRepository {
    @InjectRepository(Blog)
    private blogsRepository: Repository<Blog>;

    constructor(db: DataSource) {
        this.blogsRepository = db.getRepository(Blog);
    }

    delete(id: number) {
        return this.blogsRepository.softDelete(id);
    }

    async getAllBlogsByUser(id: number) {

        const blogs2 = await this.blogsRepository.find({
            where: {
              userId: id,
              isPublished: true
            },
        });
        console.log(blogs2);

        return blogs2;
    }
}

