import { UserRequestDto } from './dto/user-request.dto';
import { UserRepository } from './user.repository';
import { BlogRepository } from '../utils/blog.repository';
export declare class UserService {
    private usersRepository;
    private blogsRepository;
    constructor(usersRepository: UserRepository, blogsRepository: BlogRepository);
    create(userRequestDto: UserRequestDto): Promise<{
        id: number;
        username: string;
        name: string;
        role: import("./enums/role.enum").UserRole;
        deletedAt?: Date;
    }>;
    findAll(): Promise<import("./entities/user.entity").User[]>;
    findAllWithoutAdmin(): Promise<import("./entities/user.entity").User[]>;
    findOneId(id: number): Promise<import("./entities/user.entity").User>;
    findOneUsername(username: string): Promise<import("./entities/user.entity").User>;
    findOneUsernameWithPassword(username: string): Promise<import("./entities/user.entity").User>;
    remove(id: number): Promise<string>;
    update(id: number, isAdmin: boolean, isSameUser: boolean, userRequestDto: UserRequestDto): Promise<import("./entities/user.entity").User>;
}
