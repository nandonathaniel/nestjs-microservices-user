import { UserRole } from "../entities/user.entity";
export declare class UserRequestDto {
    username: string;
    password: string;
    name: string;
    role: UserRole;
}
