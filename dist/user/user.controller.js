"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserController = void 0;
const common_1 = require("@nestjs/common");
const user_service_1 = require("./user.service");
const microservices_1 = require("@nestjs/microservices");
const user_request_dto_1 = require("../dto/user-request.dto");
const role_enum_1 = require("./enums/role.enum");
let UserController = exports.UserController = class UserController {
    constructor(userService) {
        this.userService = userService;
    }
    create(userRequestDto) {
        console.log(userRequestDto);
        if (!userRequestDto.username || !userRequestDto.password || !userRequestDto.name || !userRequestDto.role) {
            throw new common_1.BadRequestException('Missing required properties');
        }
        const alphanumericRegex = /^(?=.*[a-zA-Z])[a-zA-Z0-9]+$/;
        if (!alphanumericRegex.test(userRequestDto.username)) {
            throw new common_1.BadRequestException('Username must be alphanumeric with at least one alphabetical character');
        }
        return this.userService.create(userRequestDto);
    }
    findById(id) {
        const resp = this.userService.findOneId(id);
        return resp;
    }
    findByUsername(data) {
        const regex = /^[a-zA-Z0-9]*[a-zA-Z][a-zA-Z0-9]*$/;
        if (!regex.test(data.username)) {
            return new Error('Invalid username');
        }
        return data.withPassword ? this.userService.findOneUsernameWithPassword(data.username) : this.userService.findOneUsername(data.username);
    }
    findAllUser(isAdmin) {
        return isAdmin ? this.userService.findAll() : this.userService.findAllWithoutAdmin();
    }
    update(data) {
        if (!data.isAdmin && !data.isSameUser) {
            return new common_1.UnauthorizedException('You are not authorized to update this user.');
        }
        if (!data.isAdmin && data.userRequestDto.role) {
            data.userRequestDto.role = role_enum_1.UserRole.Member;
        }
        return this.userService.update(data.id, data.isAdmin, data.isSameUser, data.userRequestDto);
    }
    remove(id) {
        return this.userService.remove(id);
    }
};
__decorate([
    (0, microservices_1.MessagePattern)('createUser'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_request_dto_1.UserRequestDto]),
    __metadata("design:returntype", void 0)
], UserController.prototype, "create", null);
__decorate([
    (0, microservices_1.MessagePattern)('findOneUserId'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", void 0)
], UserController.prototype, "findById", null);
__decorate([
    (0, microservices_1.MessagePattern)('findOneUserUsername'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], UserController.prototype, "findByUsername", null);
__decorate([
    (0, microservices_1.MessagePattern)('findAllUser'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Boolean]),
    __metadata("design:returntype", void 0)
], UserController.prototype, "findAllUser", null);
__decorate([
    (0, microservices_1.MessagePattern)('updateUser'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], UserController.prototype, "update", null);
__decorate([
    (0, microservices_1.MessagePattern)('deleteUser'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", void 0)
], UserController.prototype, "remove", null);
exports.UserController = UserController = __decorate([
    (0, common_1.Controller)(),
    __metadata("design:paramtypes", [user_service_1.UserService])
], UserController);
//# sourceMappingURL=user.controller.js.map