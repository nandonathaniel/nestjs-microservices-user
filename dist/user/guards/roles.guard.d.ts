import { ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
declare const RolesGuard_base: import("@nestjs/passport").Type<import("@nestjs/passport").IAuthGuard>;
export declare class RolesGuard extends RolesGuard_base {
    private reflector;
    constructor(reflector: Reflector);
    matchRoles(roles: string[], userRole: string): boolean;
    canActivate(context: ExecutionContext): boolean;
}
export {};
