import { UnauthorizedException } from '@nestjs/common';
import { UserService } from './user.service';
import { UserRequestDto } from '../dto/user-request.dto';
import { UserRole } from './enums/role.enum';
export declare class UserController {
    private readonly userService;
    constructor(userService: UserService);
    create(userRequestDto: UserRequestDto): Promise<{
        id: number;
        username: string;
        name: string;
        role: UserRole;
        deletedAt?: Date;
    }>;
    findById(id: number): Promise<import("./entities/user.entity").User>;
    findByUsername(data: any): Promise<import("./entities/user.entity").User> | Error;
    findAllUser(isAdmin: boolean): Promise<import("./entities/user.entity").User[]>;
    update(data: any): Promise<import("./entities/user.entity").User> | UnauthorizedException;
    remove(id: number): Promise<string>;
}
