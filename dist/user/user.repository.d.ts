import { DataSource } from 'typeorm';
import { UserRequestDto } from './dto/user-request.dto';
import { User, UserRole } from './entities/user.entity';
export declare class UserRepository {
    private usersRepository;
    constructor(db: DataSource);
    clear(): Promise<void>;
    delete(id: number): Promise<import("typeorm").UpdateResult>;
    findAll(): Promise<User[]>;
    findAllWithoutAdmin(): Promise<User[]>;
    create(userRequestDto: UserRequestDto, password: string): Promise<{
        id: number;
        username: string;
        name: string;
        role: UserRole;
        deletedAt?: Date;
    }>;
    update(id: number, userRequestDto: UserRequestDto): Promise<import("typeorm").UpdateResult>;
    findOneId(id: number): Promise<User>;
    findOneUsernameExceptPassword(username: string): Promise<User>;
    findOneUsername(username: string): Promise<User>;
}
