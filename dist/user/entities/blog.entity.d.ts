import { User } from './user.entity';
export declare class Blog {
    id: number;
    user: User;
    userId: number;
    title: string;
    content: string;
    imageUrl: string;
    imageFileName: string;
    createdDate: Date;
    isPublished: Boolean;
    deletedAt?: Date;
}
