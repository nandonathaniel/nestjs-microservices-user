import { UserRole } from '../enums/role.enum';
export declare class User {
    id: number;
    username: string;
    password: string;
    name: string;
    role: UserRole;
    deletedAt?: Date;
}
export { UserRole };
