"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserService = void 0;
const common_1 = require("@nestjs/common");
const user_repository_1 = require("./user.repository");
const bcrypt_1 = require("../utils/bcrypt");
const blog_repository_1 = require("../utils/blog.repository");
const microservices_1 = require("@nestjs/microservices");
let UserService = exports.UserService = class UserService {
    constructor(usersRepository, blogsRepository) {
        this.usersRepository = usersRepository;
        this.blogsRepository = blogsRepository;
    }
    async create(userRequestDto) {
        const password = await (0, bcrypt_1.encodePassword)(userRequestDto.password);
        return this.usersRepository.create(userRequestDto, password);
    }
    findAll() {
        return this.usersRepository.findAll();
    }
    findAllWithoutAdmin() {
        return this.usersRepository.findAllWithoutAdmin();
    }
    async findOneId(id) {
        const result = await this.usersRepository.findOneId(id);
        if (!result) {
            throw new microservices_1.RpcException(new common_1.NotFoundException("User not found"));
        }
        return result;
    }
    async findOneUsername(username) {
        const result = await this.usersRepository.findOneUsernameExceptPassword(username);
        if (!result) {
            throw new microservices_1.RpcException(new common_1.NotFoundException("User not found"));
        }
        console.log(result);
        return result;
    }
    async findOneUsernameWithPassword(username) {
        const result = await this.usersRepository.findOneUsername(username);
        console.log(result);
        if (!result) {
            throw new common_1.NotFoundException('User not found');
        }
        return result;
    }
    async remove(id) {
        const result = await this.usersRepository.delete(id);
        if (result.affected == 0) {
            throw new microservices_1.RpcException(new common_1.NotFoundException("User not found"));
        }
        else {
            const blogs = await this.blogsRepository.getAllBlogsByUser(id);
            for (const blog of blogs) {
                await this.blogsRepository.delete(blog.id);
            }
            return "Deleted User with id " + id.toString();
        }
    }
    async update(id, isAdmin, isSameUser, userRequestDto) {
        if (userRequestDto.password) {
            userRequestDto.password = await (0, bcrypt_1.encodePassword)(userRequestDto.password);
        }
        const result = await this.usersRepository.update(id, userRequestDto);
        if (result.affected == 0) {
            throw new microservices_1.RpcException(new common_1.NotFoundException("User not found"));
        }
        else {
            return this.findOneId(id);
        }
    }
};
exports.UserService = UserService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [user_repository_1.UserRepository,
        blog_repository_1.BlogRepository])
], UserService);
//# sourceMappingURL=user.service.js.map