"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserRole = void 0;
var UserRole;
(function (UserRole) {
    UserRole["Admin"] = "admin";
    UserRole["Member"] = "member";
})(UserRole || (exports.UserRole = UserRole = {}));
//# sourceMappingURL=role.enum.js.map