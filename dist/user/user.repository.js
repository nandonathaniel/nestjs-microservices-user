"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserRepository = void 0;
const typeorm_1 = require("@nestjs/typeorm");
const common_1 = require("@nestjs/common");
const typeorm_2 = require("typeorm");
const user_entity_1 = require("./entities/user.entity");
let UserRepository = exports.UserRepository = class UserRepository {
    constructor(db) {
        this.usersRepository = db.getRepository(user_entity_1.User);
    }
    async clear() {
        console.log("user ke clear");
        const allUsers = await this.usersRepository.find();
        await this.usersRepository.remove(allUsers);
    }
    delete(id) {
        return this.usersRepository.softDelete(id);
    }
    findAll() {
        return this.usersRepository.find({ select: ['id', 'username', 'name', 'role'] });
    }
    findAllWithoutAdmin() {
        return this.usersRepository.find({ where: { role: user_entity_1.UserRole.Member }, select: ['id', 'username', 'name', 'role'] });
    }
    create(userRequestDto, password) {
        const user = this.usersRepository.create(Object.assign(Object.assign({}, userRequestDto), { password }));
        return this.usersRepository
            .save(user)
            .then((_a) => {
            var { password } = _a, result = __rest(_a, ["password"]);
            return result;
        });
    }
    update(id, userRequestDto) {
        const result = this.usersRepository.update({ id }, userRequestDto);
        return result;
    }
    findOneId(id) {
        return this.usersRepository.findOne({ where: { id: id }, select: ['id', 'username', 'name', 'role'] });
    }
    findOneUsernameExceptPassword(username) {
        return this.usersRepository.findOne({
            where: {
                username: (0, typeorm_2.Raw)(alias => `BINARY ${alias} = :username`, { username })
            },
            select: ['id', 'username', 'name', 'role']
        });
    }
    findOneUsername(username) {
        return this.usersRepository.findOne({
            where: {
                username: (0, typeorm_2.Raw)(alias => `BINARY ${alias} = :username`, { username })
            }
        });
    }
};
__decorate([
    (0, typeorm_1.InjectRepository)(user_entity_1.User),
    __metadata("design:type", typeorm_2.Repository)
], UserRepository.prototype, "usersRepository", void 0);
exports.UserRepository = UserRepository = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [typeorm_2.DataSource])
], UserRepository);
//# sourceMappingURL=user.repository.js.map