import { UserRole } from "../user/enums/role.enum";
export declare class UserRequestDto {
    username: string;
    password: string;
    name: string;
    role: UserRole;
}
