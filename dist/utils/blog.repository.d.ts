import { DataSource } from 'typeorm';
import { Blog } from '../user/entities/blog.entity';
export declare class BlogRepository {
    private blogsRepository;
    constructor(db: DataSource);
    delete(id: number): Promise<import("typeorm").UpdateResult>;
    getAllBlogsByUser(id: number): Promise<Blog[]>;
}
