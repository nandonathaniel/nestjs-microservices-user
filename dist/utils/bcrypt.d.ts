export declare function encodePassword(rawPassword: string): Promise<string>;
export declare function comparePasswords(rawPassword: string, hash: string): any;
