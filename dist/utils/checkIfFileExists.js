"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.checkAndMove = void 0;
const fs = require("fs");
async function checkIfFileExists(filePath) {
    return fs.existsSync(filePath);
}
async function checkAndMove(imageId) {
    const sourcePath = `storage/temporary/${imageId}`;
    const targetPath = `storage/persistent/${imageId}`;
    if (checkIfFileExists(sourcePath)) {
        await fs.promises.rename(sourcePath, targetPath);
        return true;
    }
    else {
        return false;
    }
}
exports.checkAndMove = checkAndMove;
//# sourceMappingURL=checkIfFileExists.js.map