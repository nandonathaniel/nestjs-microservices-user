"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.comparePasswords = exports.encodePassword = void 0;
const bcrypt = require("bcrypt");
async function encodePassword(rawPassword) {
    const salt = await bcrypt.genSaltSync();
    return bcrypt.hash(rawPassword, salt);
}
exports.encodePassword = encodePassword;
function comparePasswords(rawPassword, hash) {
    return bcrypt.compareSync(rawPassword, hash);
}
exports.comparePasswords = comparePasswords;
//# sourceMappingURL=bcrypt.js.map